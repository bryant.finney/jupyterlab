# ---
# jupyter:
#   jupytext:
#     formats: py:percent
#     text_representation:
#       extension: .py
#       format_name: percent
#       format_version: '1.3'
#       jupytext_version: 1.4.2
#   kernelspec:
#     display_name: Python 3
#     language: python
#     name: python3
# ---

# %% [markdown]
# # Default Notebook Imports
#
# The following imports are generally useful and stuff.

# %%
import numpy as np
import pandas as pd
from dateutil.relativedelta import relativedelta
from IPython.display import Markdown
from matplotlib import pyplot as plt
