# ---
# jupyter:
#   jupytext:
#     formats: py:percent
#     text_representation:
#       extension: .py
#       format_name: percent
#       format_version: '1.3'
#       jupytext_version: 1.4.2
#   kernelspec:
#     display_name: Python 3
#     language: python
#     name: python3
# ---

# %% [markdown]
# # Default Notebook Settings/Options
#
# These settings are generally useful and are applied globally.

# %%
pd.options.display.max_rows = 100
plt.style.use("dark_background")
