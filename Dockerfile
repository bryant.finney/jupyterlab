FROM jupyter/scipy-notebook

WORKDIR /home/$NB_USER/work/
COPY ./Pipfile* ./

RUN pip install --upgrade pip \
    && pip install --upgrade pipenv \
    && pipenv install --system

ENV JUPYTER_ENABLE_LAB=yes, CHOWN_HOME=yes

RUN ["jupyter", "labextension", "install", "-y", "--clean", "@jupyterlab/git", \
    "@jupyterlab/toc", "@ryantam626/jupyterlab_code_formatter", "jupyterlab-drawio", \
    "jupyterlab-jupytext", "jupyterlab-spreadsheet", "nbdime-jupyterlab"]

RUN [ "jupyter", "nbextension", "enable", "--py", "jupytext" ]
RUN [ "jupyter", "serverextension", "enable", "--py", "jupyterlab_code_formatter" ]

WORKDIR /home/$NB_USER

COPY ./opt/conda/etc/jupyter /opt/conda/etc/jupyter

COPY ./home/$NB_USER /home/$NB_USER

USER root
RUN chown -R $NB_UID:$NB_GID /home/$NB_USER

VOLUME ["/home/$NB_USER/work"]